const Task = require("../Models/task.js")

/*Controllers and function*/

//Controller/function to get all task on our database
module.exports.getAll = (request, response) => {

	Task.find({}).then(result => {
		//to capture the result of the find method
		return response.send(result)
	})
	//.catch method captures the error when the find method is executed.
	.catch(error => {
		return response.send(error);
	})

}

//Add Task on our Database
module.exports.createTask = (request, response) => {
	const input = request.body;

	Task.findOne({name: input.name})
	.then(result => {
		if (result !== null){
			return response.send("The task is already Exist!")
		}else{
			let newTask = new Task(
				{
					name: input.name
				}
			)
				newTask.save().then(save => {
					return response.send("The task is successfully added!");
				})
			.catch(error => {
					return response.send(error);
				})
		};

		
	})
	.catch(error => {
		return response.send(error)
	})
}

//Controller that will dellete the document that contains
module.exports.deleteTask = (request, response) => {
	let idToBeDeleted = request.params.id;

	//findbyIdAndRemove - to find the document that contains the id and then delete the document
	Task.findByIdAndRemove(idToBeDeleted)
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}

//Activity
module.exports.getTask = (request, response) => {
	let idToBeGet = request.params.id;

	Task.findById(idToBeGet)
	.then(result => {
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}

module.exports.updateTask = (request, response) =>{
	let idToBeUpdated = request.params.id;

	Task.findByIdAndUpdate(idToBeUpdated,{status: "complete"})
	.then(result =>{
		return response.send(result)
	})
	.catch(error => {
		return response.send(error)
	})
}


const express = require("express")
const router = express.Router()

const taskController = require("../Controllers/taskController.js")

//route for getAll
router.get("/", taskController.getAll)

//route for createTask
router.post("/addTask", taskController.createTask)

//route for delete
router.post("/deleteTask/:id", taskController.deleteTask)

//route for the activity
router.get("/getTask/:id", taskController.getTask)

//route for the activity using update method
router.put("/updateTask/:id/complete", taskController.updateTask)

//export the module
module.exports = router